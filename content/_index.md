---
title: Welcome !
subtitle: ... on yet another blog on cybersecurity
---

## This is mh4ckt3mh4ckt1c4s' blog

I write on this blog everything related to my trip in the wonderful world of Computer Science, Networking and Cybersecurity.
In particular, you will find a lot of information concerning :
* The [projects](/projects) I'm participating in, or participated in 
* Some things I write about on my [blog](/blog)

If you want to learn more about who I am and what I'm doing, you can go [here](/about).

I'm writing this blog for two people:
* For myself, in order to organize and think again about everything I learn, to list all the tools I like but whose I'm forgetting the name every week or so (I *know* it's your case too, don't pretend), and to write down a few tricks I find here and there.
* For you, reader, in an attempt to share with you my knowledge, hoping that you will make good use of it.

Good reading and happy hacking!