---
title: "Projects"
---
## My projects, all around

Here I try to keep an updated list of the projects I participate or participated in, in order to remember where I went, what I've done, and where I'm going.

### ArchLinux

I use ArchLinux a lot, and in order to bring my contribution, I adopted [some AUR packages](https://aur.archlinux.org/packages?K=mh4ckwascut&SeB=m). This allows me to learn more about maintaining packages, and compilation / installation / packaging topics. I mainly package binaries that I use for my PhD, as they are often new and not used a lot. Maintaining these packages allow me to quickly set up experimentation platforms based on Arch, as all the needed binaries are already packaged and up to date.

One day, I was working on LLVM and I noticed that the LLVM distribution of Arch was out of date. This prompted me to become a [Package Maintainer](https://archlinux.org/people/package-maintainers/#mh4ckt3mh4ckt1c4s) in order to help out with the official packages of Arch Linux, and by the same time promote the popular packages that I am maintaining in the AUR to official packages.

### Teaching

As a PhD student I am sometimes teaching courses on various topics revolving around cybersecurity.

### Research 

I was President of my school's cybersecurity club ([HackademINT](https://www.hackademint.org)) in 2021-2022, and I tried to make people aware of the importance of cybersecurity, and train those want how this attacks work. For this, we (me and the awesome members of the club) proposed a formation each Tuesday evening except during student vacations, with a different topic each time, and we publish our formations [here](https://www.hackademint.org/formations).

I also created the first edition of the 404 CTF TODO link, creating cybersecurity challenges for all levels and bringing them to thousands of people.

### Open source

I try to be active and to help the open-source community as much as I can, with opening issues and writing Pull Requests if I know how to fix the bugs I find. This allows me to take part in the FLOSS community I love and support, and to learn a lot about different languages, good code practice and bug finding.