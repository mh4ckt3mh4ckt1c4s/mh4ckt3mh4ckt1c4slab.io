---
title: "The training websites"
subtitle: "Where to go to practice your skills"
date: 2021-11-24
---

You'll find here all the websites I go on to test my skills on dedicated cybersecurity challenges. There is a lot of them, all with specificities, and when you try to begin in cybersecurity you don't really know which site to use, so I thought it was a good idea to list all that I use and compare them.

## Root-me.org, french leader and excellent training site

[root-me](https://root-me.org) is a French hacking training website with a lot of categories. Inscription is free and challenges are all available for everybody (maybe not the last ones, but you just have to wait for a few months)

All levels are present, and a lot of categories, but if you want to learn from scratch (meaning without basic programming / networking skills), maybe it's not the best website to start. The learning curve is steep and the resources not perfectly appropriate. But if you know the basis and how to google a problem, and you want to practice, this site is made for you.

Along with the challenges, root-me have a strong (french) community. It's nice if you speak French natively and not fluent in English, you won't have to made the additional effort to translate (even though I strongly encourage you to master English). For those who don't speak French, don't worry, the majority of the community speaks also English.

## TryHackMe, where to start if you know nothing at all and want to discover

I only went a few times on [TryHackMe](https://tryhackme.com/) as I discovered this site long after my beginning, but I'd started by it if I had learned about it before. You really start from the basis and have a lot of explanations and exercises along the way. If you already have some knowledge the beginning can seems dumb, but it's really a good way to learn. 

One downside is the fact that some modules are not free (but hey, you can't blame those who created this website to want to make some money). It can be a bit frustrating if the specific lesson you wanted to learn about is unavailable, but you can still pay or look elsewhere.

I didn't use TryHackMe long enough yet to learn about its community, but I heard its members are numerous and active.

## Hack The Box, international training website

I went on [Hack The Box](https://www.hackthebox.com/) nearly a year after starting to learn cybersecurity actively, in order to try the pentesting. And I loved it. Rooting a full machine is way more fascinating from my point of view than just a single challenge. The system of Hack The Box is simple : as a free account, you only have access to "active" boxes (full machines for pentest) and challenges (yes, there's challenges too), and if you want to try the retired ones, you have to pay. This is interesting as the active challenges are replaced continuously, ensuring that even with a free account, you'll always have something to do. 

They also provide tougher challenges with a whole infrastructure to root (you have to unlock these by gaining in level), and also CTFs alongside with different types of format and target, so if you're interested you just have to keep an eye on their announcements.

Their community is also really active, and keen to help. They also work this some known people in the cybersecurity domain, which is interesting if you have the opportunity to read them about one challenge or something.

## CryptoHack, only for fans of cryptography

I discovered [CryptoHack](https://cryptohack.org/) very recently, but I have to say it's a wonderful website. I really love math so the cryptography is naturally attractive for me. Unfortunately, as in all domains of cybersecurity, it is very difficult to start if you don't know where to begin and where to find the good resources to learn. CryptoHack is a real remedy for this. Along with challenges, you have mini-courses to explain you step by step a technology or a vulnerability. And what more, it's completely free !

CryptoHack is especially great for its learning curve : even if you know nothing about cryptography, you'll be able really easily, with explanations covering both theoric and practical aspects (such as how to manipulate cryptographic functions with Python). For those more advanced, you'll be able to skip this and dive into the advanced math behind cryptography, or the inner working of the AES, and other really cool stuff !

Also, their community is present on discord and rapidly growing. I didn't discuss with it yet, but it seems that there is some very competent people present, so you can ask help with a challenge or simply discuss crypto with professionals.

## pwn.college, a very smooth introduction to binary exploitation

I sadly discovered [pwn.college](https://dojo.pwn.college) well after I first tried to learn pwn. This website is really starting by the basics and allow you to progress very smoothly (maybe too smoothly, depending on if you already have experience or not) in the domain of binary exploitation. Furthermore, this website is originally the support for a cybersecurity teacher, so all the modules are accompanied by YouTube videos and slides explaining the topic. 

One of the disadvantages of the platform is that it can be quite slow from time to time. But it is due to one of its biggest advantages : you will always be alone on the challenges, isolated in a container dedicated to yourself. This is a huge plus as you won't be annoyed by other people working on the same binary as you, which can be troublesome from time to time. Also, this platform is completely free and accessible to everyone, and you have to recognize the wonderful work of the teacher who created this.

I don't know about the community of this platform as I never interacted with it, but there is a Discord server available for those who want to discuss the challenges or get some help, which is better than nothing.

## Conclusion

Because I like organized things, there is a little array recapitulating everything above (and more) :

|Platform|Speciality|Difficulty|Access|Community|Good stuff|Things I dislike|
|--|--|--|--|--|--|--|
|[root-me.org](https://root-me.org)|No|All|Nearly everything free|Big (french) but english also possible|A lot of content|Some challenges outdated or broken|
|[TryHackMe](https://tryhackme.com)|No|Beginner to medium|Free content, paying VIP access|Big|Very well done for beginners, a lot of explanations|Difficult to really focus with a free account|
|[HackTheBox](https://www.hackthebox.com) |No (pentest)|Good beginner to insane|Free content, paying VIP access|Big and nice|Axed on pentest/realism|With a free account working with other people can lead to spoil|
|[CryptoHack](https://cryptohack.org)|Cryptography|Some knowledge required in programming/maths|Completely free|Medium|Very progressive and interesting challenge lists|Progress can be steep|
|[pwn.college](https://dojo.pwn.college)|pwn (binary exploitation)|Beginner to medium|Completely free|Unknown|Challenges are very progressive and lessons available|Can be sometimes slow|