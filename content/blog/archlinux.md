---
title: "My ArchLinux setup"
subtitle: "What I use for my custom OS"
date: 2021-11-24
---

I tested many OSes before choosing to try Archlinux : my main goal then was to learn the inner workings of an OS and the interactions between the many components by installing and configuring them one by one. After a few months trying (and configuring) it, I really loved it, and now I'm using Arch as my OS. I'm listing here all the components I assembled in order to create myself a fully functional and custom OS.

As I am really curious and I like to try new things, my Archlinux is constantly evolving, and this page will evolve along it.  

## Before starting to build your OS

### The ArchLinux wiki

It can seem stupid, but this is one of the most useful resource I ever found. The [arch wiki](https://wiki.archlinux.org/) is filled with a ton of valuable information, either if you're using Arch or not. If you struggle to configure something when building you OS (network, trouble with systemctl or grub...), maybe the wiki have the answer. Don't forget that in a good lot of cases, you're struggling against a known issue or misconfiguration more experimented people have already encountered and solved.

### The installation guide

If you want to start from Archlinux, you first have to install it. You may already know it, this has nothing to compare with other Linux installations such as Ubuntu or Debian where you only have to type Enter most of the time. If you're not familiar with Linux systems, I strongly advise you to start by the basics and learn to use it, or you may never succeed in installing Arch. If you already are familiar and just want to try something different, just dive ! You should learn a lot of things and have a lot of fun (a little pain also maybe, but that's worth it).

The [installation guide](https://wiki.archlinux.org/title/Installation_guide) is (I think) the best way to install Archlinux. By following it, you will :
- Learn a lot on how your system is working
- (Nearly) assure yourself that your Archlinux will start once the installation complete
- Be able to troubleshoot all the problems you may encounter by using the wiki and all the links provided.

I suggest you to try to understand all the commands you will type, and not only copy it stupidly. It will serve you well in the future, I can guarantee it !


## Main OS

### Xorg

When you want to use a GUI on a fresh Archlinux install, you have to choose a display server first. Basically, you have the choice between [Xorg](https://wiki.archlinux.org/title/xorg), the historic one and the most used, and [Wayland](https://wiki.archlinux.org/title/Wayland), never, simpler and more secure, and aimed to become the successor of Xorg. I choose to use Xorg because I was a beginner and there is a lot more documentation and community around Xorg than around Wayland. However I intend to try and maybe switch to Wayland one day.

### lightdm and lightlocker

The fist thing I see when I'm starting my PC (after `grub`) is my display manager, `lightdm`. I chose it because it's light, simple and easy to configure. Lightdm then start my i3 session. Alongside it, I'm using `light-locker` to lock my session on demand or when I close my laptop. No specific configuration is required, just launch `light-locker` on startup, and you're set.
### i3gaps

[i3gaps](https://github.com/Airblader/i3) is a Windows Manager (a fork of [i3wm](https://i3wm.org/) with more functionalities). As I learned to work more and more with the terminal and spent more time coding, I wanted to use a lighter, more configurable interface. I stumbled upon i3 and after a time of adaptation, I like it very much as it makes me really faster in my work.

### polybar

I really like to have a lot of easily accessible information when I am working on something, so I wanted to have a beautiful and highly configurable taskbar to work with i3. [Polybar](https://github.com/polybar/polybar) does exactly that, and the possibilities of personalization are nearly unlimited. I add or change some functionalities from time to time on mine, it's easy and always comes handy.

### rofi

[rofi](https://github.com/davatorium/rofi) is my application launcher. I was bored by dmenu where I was struggling to find the good application, or I was not remembering the good name (eg thunar for the file browser). I searched for an alternative and discovered rofi, which better suited my needs. I now always use it to launch all my apps.

### nitrogen

I like i3 a lot, but I was quickly bored with its black wallpaper. I searched for a possibility to display a simple wallpaper and found [nitrogen](https://wiki.archlinux.org/title/nitrogen). Really simple to use, as I only wanted a static background it was perfect for me.

### yay

One of the strongest points of ArchLinux is its [AUR](https://aur.archlinux.org/) : a list of community-driven packages coming along the official ones. It allows you to install and manage nearly any of the tools, binaries and drivers you'll ever need with one tool and to maintain them up-to-date very easily.

## Utilities

### arandr

Standard tool allowing me to share easily my screen via HDMI, available [here](https://github.com/haad/arandr) and on the AUR.

### nm-connection-editor

Easy way to manage all my internet connections, both wired and Wi-Fi. Along with `nm-manager-applet` which are displaying a simple and accessible icon on my status bar (polybar), it allows me to deal with my networks very quickly.

### pulseaudio

Standard GUI to manage my sound settings.

### Flameshot

Very good screenshot software. Too many options for my taste, but I configured it to remove those I was not using. I bound it to a keyboard shortcut with i3 and now I'm using it every day.

### thunar

Most of the time I am using the CLI to manage my files, but sometimes it is easier using a GUI. When I need it I am using thunar, a lightweight and simple files explorer.

### deadd-notification-center

One thing I missed the most when using i3 is the possibility to see and access all my notifications on my screen, regardless of which application sent it and on which screen it is displayed. Using `deadd-notification-center` solved this problem.

