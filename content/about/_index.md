---
Title: About me
---

My name is Quentin, but I go by the pseudo of mh4ckt3mh4ckt1c4s (or sometimes mh4ckwascut, if the site I signed up on wasn't keen to accept my pseudo's length).

I'm a cybersecurity enthusiast and love science in general, and computer science in particular. I always look for something new to learn, and that's why I'm fond of hacking. I'm a [PhD student](https://theses.fr/s374883) at [Télécom SudParis](https://www.telecom-sudparis.eu/en/about-us/about-telecom-sudparis/) of the [Institut Polytechnique de Paris](https://www.ip-paris.fr/en/about/about-us). I also participate in helping others to learn hacking with my school's cybersecurity club [HackademINT](https://www.hackademint.org), of which I was the President in 2021/2022.

I practice a lot on [root-me](https://www.root-me.org/mh4ckt3mh4ckt1c4s), [Hack The Box](https://app.hackthebox.com/profile/436328) and [CryptoHack](https://cryptohack.org/user/mh4ckwascut/), and I'm progressing every day.

I created this site in order to lay down all I have learned and also, I hope, to help some people on their way to learning about computer science and cybersecurity.

Good visit and happy hacking!

<a href="https://app.hackthebox.com/profile/436328">![Hack The Box badge](https://www.hackthebox.com/badge/image/436328)</a>

